# Routes Config File
Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :product
  resources :customer
  resources :order
  resources :user
  resources :product_category
  root 'product#index'
end