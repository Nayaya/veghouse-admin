class Customer < ApplicationRecord
    has_many :orders

    scope :sorted, lambda { order("customers.id ASC") }
    scope :search, lambda {|query| where(["first_name LIKE ?", "%#{query}%"])}
    scope :newest_first, lambda { where("customers.created_at DESC") }
end
