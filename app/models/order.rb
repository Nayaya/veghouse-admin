class Order < ApplicationRecord
    belongs_to :customer
    belongs_to :user
    has_many :products

    scope :sorted, lambda { order("orders.id ASC") }
    scope :search, lambda {|query| where(["description LIKE ?", "%#{query}%"])}
    scope :newest_first, lambda { where("orders.created_at DESC") }
end
