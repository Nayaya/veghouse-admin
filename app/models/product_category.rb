class ProductCategory < ApplicationRecord
    has_many :products

    scope :sorted, lambda { order("product_categories.id ASC") }
    scope :search, lambda {|query| where(["title LIKE ?", "%#{query}%"])}
    scope :newest_first, lambda { where("product_categories.created_at DESC") }
end
