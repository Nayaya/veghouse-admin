class User < ApplicationRecord
    has_many :orders

    scope :sorted, lambda { order("users.id ASC") }
    scope :search, lambda {|query| where(["first_name LIKE ?", "%#{query}%"])}
    scope :newest_first, lambda { where("users.created_at DESC") }
end
