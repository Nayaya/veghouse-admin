class Product < ApplicationRecord
    belongs_to :product_category
    belongs_to :order

    scope :sorted, lambda { order("products.id ASC") }
    scope :search, lambda {|query| where(["title LIKE ?", "%#{query}%"])}
    scope :newest_first, lambda { where("products.created_at DESC") }
    # Ex:- scope :active, -> {where(:active => true)}
end
