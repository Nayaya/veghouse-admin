class OrderController < ApplicationController
  layout "admin"
  def index
    @orders = Order.sorted
  end

  def show
    @order = Order.find(params[:id])
  end

  def create
    @order = Order.new(order_params)
    if @order.save
      redirect_to(:action => "index")
    else
      render("new")
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    redirect_to(:action => "index")
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    if @order.update_attributes(order_params)
      redirect_to(:action => "index")
    else
      render("edit")
    end
  end

  private
    def order_params
      params.require(:order).permit(:description,)
    end
end
