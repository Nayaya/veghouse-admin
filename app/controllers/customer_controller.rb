class CustomerController < ApplicationController
  layout "admin"
  def index
    @customers = Customer.sorted
  end

  def show
    @customer = Customer.find(params[:id])
  end

  def create
    @customer = Customer.new(customer_params)
    if @customer.save
      redirect_to(:action => "index")
    else
      render("new")
    end
  end

  def destroy
    @customer = Customer.find(params[:id])
    @customer.destroy
    redirect_to(:action => "index")
  end

  def edit
    @customer = Customer.find(params[:id])
  end

  def update
    @customer = Customer.find(params[:id])
    if @customer.update_attributes(customer_params)
      redirect_to(:action => "index")
    else
      render("edit")
    end
  end
  private
    def customer_params
      params.require(:customer).permit(:first_name, :last_name, :phone, :email, :address, :local_govt, :state)
    end
end
