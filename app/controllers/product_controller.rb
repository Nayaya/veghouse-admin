class ProductController < ApplicationController
  layout "admin"

  def index
    @products = Product.sorted
    logger.debug
  end

  def show
    @product = Product.find(params[:id])
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to(:action => "index")
    else
      render("new")
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to(:action => "index")
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(product_params)
      redirect_to(:action => "index")
    else
      render("edit")
    end
  end

  private
    def product_params
      params.require(:product).permit(:title, :price, :description, :image_url)
    end
end
