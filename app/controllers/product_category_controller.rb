class ProductCategoryController < ApplicationController
  layout "admin"
  def index
    @product_categories = ProductCategory.sorted
  end

  def show
    @product_category = ProductCategory.find(params[:id])
  end

  def create
    @product_category = ProductCategory.new(product_params)
    if @product_category.save
      redirect_to(:action => "index")
    else
      render("new")
    end
  end

  def destroy
    @product_category = ProductCategory.find(params[:id])
    @product_category.destroy
    redirect_to(:action => "index")
  end

  def edit
    @product_category = ProductCategory.find(params[:id])
  end

  def update
    @product_category = ProductCategory.find(params[:id])
    if @product_category.update_attributes(product_params)
      redirect_to(:action => "index")
    else
      render("edit")
    end
  end
  private
    def product_params
      params.require(:product_category).permit(:title)
    end
end
