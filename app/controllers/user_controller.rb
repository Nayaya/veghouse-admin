class UserController < ApplicationController
  layout "admin"
  def index
    @users = User.sorted
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to(:action => "index")
    else
      render("new")
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to(:action => "index")
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to(:action => "index")
    else
      render("edit")
    end
  end

  private
    def user_params
      params.require(:user).permit(:first_name, :last_name, :phone, :email, :address, :local_govt, :state)
    end
end
