class CreateCustomers < ActiveRecord::Migration[6.0]
  def up
    create_table :customers do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :address
      t.string :state
      t.string :local_govt
      t.references :order

      t.timestamps
    end

    def def down 
      drop_table :customers
    end
  end
end
