class CreateOrders < ActiveRecord::Migration[6.0]
  def up
    create_table :orders do |t|
      t.string :description
      t.references :customer
      t.references :user
      t.timestamps
    end
  end

  def def down 
    drop_table :orders
  end
end
