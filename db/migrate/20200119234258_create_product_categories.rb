class CreateProductCategories < ActiveRecord::Migration[6.0]
  def up
    create_table :product_categories do |t|
      t.string :title
      t.references :product
      t.timestamps
    end
  end

  def down 
    drop_table :product_categories
  end
end
