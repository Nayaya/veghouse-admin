class CreateUsers < ActiveRecord::Migration[6.0]
  def up
    create_table :users do |t|
      t.string :role
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :address
      t.string :state
      t.string :local_govt
      t.timestamps
    end
  end

  def def down 
    drop_table :users
  end
end
