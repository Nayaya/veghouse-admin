class CreateProducts < ActiveRecord::Migration[6.0]
  def up
    create_table :products do |t|
      t.string "title"
      t.float  "price"
      t.string  "description"
      t.string "image_url"
      t.references :product_category
      t.timestamps
    end
  end

  def def down 
    drop_table :products
  end
end
